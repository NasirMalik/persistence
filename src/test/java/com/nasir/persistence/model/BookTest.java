package com.nasir.persistence.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import static org.junit.jupiter.api.Assertions.*;

class BookTest {

    EntityManager em;
    EntityTransaction tx;

    @BeforeEach
    void setUp() {
        em = Persistence.createEntityManagerFactory("testjpa-unit").createEntityManager();
        tx = em.getTransaction();
    }

    @AfterEach
    void tearDown() {
        em.close();
    }

    @Test
    void shouldCreate() {
        Book book = new Book("Test Book 1", "Long Description for the Book", 35F, "345345345345", (short) 450);

        tx.begin();
        em.persist(book);
        tx.commit();
        System.out.println(book.getId());
    }
}
package com.nasir.persistence.model;

import com.nasir.persistence.listeners.ValidationListener;
import com.nasir.persistence.model.id.ArtistId;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.PostPersist;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@EntityListeners({
        ValidationListener.class
})
public class Musician extends Artist {

    private LocalDate dob;
    private Float duration;

    @NotNull(message = "Rate cannot be null")
    private Float rate;

    public Musician() {
    }

    public Musician(String firstName, String lastName, String biography, LocalDate dob, Float duration) {
        this.id = new ArtistId();
        this.id.setFirstName(firstName);
        this.id.setLastName(lastName);
        this.biography = biography;
        this.dob = dob;
        this.duration = duration;
    }

    public LocalDate getDob() {
        return dob;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }

    public Float getDuration() {
        return duration;
    }

    public void setDuration(Float duration) {
        this.duration = duration;
    }

    public Float getRate() {
        return rate;
    }

    public void setRate(Float rate) {
        this.rate = rate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Musician musician = (Musician) o;

        if (!id.getFirstName().equals(musician.id.getFirstName())) return false;
        if (!id.getLastName().equals(musician.id.getLastName())) return false;
        if (!biography.equals(musician.biography)) return false;
        return duration.equals(musician.duration);
    }

    @Override
    public int hashCode() {
        int result = id.getFirstName().hashCode();
        result = 31 * result + id.getLastName().hashCode();
        result = 31 * result + biography.hashCode();
        result = 31 * result + duration.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Musician{" +
                "firstName='" + id.getFirstName() + '\'' +
                ", lastName='" + id.getLastName() + '\'' +
                ", biography='" + biography + '\'' +
                ", dob=" + dob +
                ", duration=" + duration +
                '}';
    }

    @PostPersist
    public void display() {
        System.out.println(id + " : " + toString());
    }

}

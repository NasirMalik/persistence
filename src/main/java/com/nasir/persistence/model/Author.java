package com.nasir.persistence.model;

import com.nasir.persistence.model.id.ArtistId;

import javax.persistence.Entity;
import java.util.Objects;

@Entity
public class Author extends Artist {

    private Float rate;
    private Short noOfBooks;

    public Author() {
    }

    public Author(String firstName, String lastName, String biography, Float rate, String contact, Short noOfBooks) {
        this.id = new ArtistId();
        this.id.setFirstName(firstName);
        this.id.setLastName(lastName);
        this.biography = biography;
        this.rate = rate;
        this.contact = contact;
        this.noOfBooks = noOfBooks;
    }

    public Float getRate() {
        return rate;
    }

    public void setRate(Float rate) {
        this.rate = rate;
    }

    public Short getNoOfBooks() {
        return noOfBooks;
    }

    public void setNoOfBooks(Short noOfBooks) {
        this.noOfBooks = noOfBooks;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Author author = (Author) o;
        return id.getFirstName().equals(author.id.getFirstName()) &&
                id.getLastName().equals(author.id.getLastName()) &&
                biography.equals(author.biography) &&
                rate.equals(author.rate) &&
                contact.equals(author.contact) &&
                noOfBooks.equals(author.noOfBooks);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id.getFirstName(), id.getLastName(), biography, rate, contact, noOfBooks);
    }

    @Override
    public String toString() {
        return "Author{" +
                "firstName='" + id.getFirstName() + '\'' +
                ", lastName='" + id.getLastName() + '\'' +
                ", biography='" + biography + '\'' +
                ", rate=" + rate +
                ", contact='" + contact + '\'' +
                ", noOfBooks=" + noOfBooks +
                '}';
    }
}

package com.nasir.persistence.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@NamedQueries( {
        @NamedQuery(name = "ALL", query = "select c from CD c"),
        @NamedQuery(name = "CD", query = "select c from CD c where c.id = ?1")
})
public class CD extends Item {

    private String title;
    private String genre;
    private Float duration;

    @OneToMany(mappedBy = "id.cd", cascade = CascadeType.ALL)
    private Set<Edition> editions = new HashSet<>();

    // will create a FK with default name
    @OneToOne(cascade = CascadeType.ALL)
    // will override the FK default name
    @JoinColumns (value = {
        @JoinColumn(name = "musician_fName", referencedColumnName = "firstName"),
        @JoinColumn(name = "musician_lName", referencedColumnName = "lastName")
    })
    private Musician leadMusician;

    /*
        // will by default map by a JOINTABLE
        @OneToMany

        // overrides the default behaviour by a FK in the many table
        @JoinColumn(name = "cd_id")
     */

    // will by default map by a JOINTABLE
    @ManyToMany(cascade = CascadeType.ALL)
    // overrides the default behaviour by customising JOINTABLE
    @JoinTable(name = "cd_musician",
        joinColumns = @JoinColumn(name = "cd_id"),
        inverseJoinColumns = {
            @JoinColumn (name="musician_fName", referencedColumnName="firstName"),
            @JoinColumn (name="musician_lName", referencedColumnName="lastName")
        }
    )
    private Set<Musician> musicians = new HashSet<>();

    public CD() {
    }

    public CD(String title, String description, String genre, Float duration) {
        this.title = title;
        this.description = description;
        this.genre = genre;
        this.duration = duration;
    }

    public Set<Edition> getEditions() {
        return editions;
    }

    public void setEditions(Set<Edition> editions) {
        this.editions = editions;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Float getDuration() {
        return duration;
    }

    public void setDuration(Float duration) {
        this.duration = duration;
    }

    public Musician getLeadMusician() {
        return leadMusician;
    }

    public void setLeadMusician(Musician leadMusician) {
        this.leadMusician = leadMusician;
    }

    public Set<Musician> getMusicians() {
        return musicians;
    }

    public void setMusicians(Set<Musician> musicians) {
        this.musicians = musicians;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CD cd = (CD) o;

        if (!title.equals(cd.title)) return false;
        if (!description.equals(cd.description)) return false;
        if (!genre.equals(cd.genre)) return false;
        return (!duration.equals(cd.duration));
    }

    @Override
    public int hashCode() {
        int result = title.hashCode();
        result = 31 * result + description.hashCode();
        result = 31 * result + genre.hashCode();
        result = 31 * result + duration.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "CD{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", genre='" + genre + '\'' +
                ", duration=" + duration +
                ", leadMusician=" + leadMusician +
                '}';
    }

    @PostLoad
    public void loadEditions() {
        this.getEditions();
    }
}

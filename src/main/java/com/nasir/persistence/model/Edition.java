package com.nasir.persistence.model;

import com.nasir.persistence.model.id.EditionId;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Entity
public class Edition {

    @EmbeddedId
    private EditionId id;

    private String detail;
    private LocalDate releaseDate;

    public Edition() {
    }

    public Edition(EditionId id, String detail, LocalDate releaseDate) {
        this.id = id;
        this.detail = detail;
        this.releaseDate = releaseDate;
    }

    public Edition(Integer editionNumber, String detail, LocalDate releaseDate) {
        this.id = new EditionId();
        this.id.setEditionNumber(editionNumber);
        this.detail = detail;
        this.releaseDate = releaseDate;
    }

    public EditionId getId() {
        return id;
    }

    public void setId(EditionId id) {
        this.id = id;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Edition edition = (Edition) o;
        return Objects.equals(id, edition.id) &&
                Objects.equals(detail, edition.detail) &&
                Objects.equals(releaseDate, edition.releaseDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, detail, releaseDate);
    }

    @Override
    public String toString() {
        return "Edition{" +
                "id=" + id +
                ", detail='" + detail + '\'' +
                ", releaseDate=" + releaseDate +
                '}';
    }
}

package com.nasir.persistence.model;

import javax.persistence.Entity;

@Entity
public class Book extends Item {

    private String name;
    private String isbn;
    private Short noOfPages;


    public Book() {
    }

    public Book(String name, String description, Float cost, String isbn, Short noOfPages) {
        this.name = name;
        this.description = description;
        this.cost = cost;
        this.isbn = isbn;
        this.noOfPages = noOfPages;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public Short getNoOfPages() {
        return noOfPages;
    }

    public void setNoOfPages(Short noOfPages) {
        this.noOfPages = noOfPages;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        if (id != null ? !id.equals(book.id) : book.id != null) return false;
        if (name != null ? !name.equals(book.name) : book.name != null) return false;
        if (description != null ? !description.equals(book.description) : book.description != null) return false;
        if (cost != null ? !cost.equals(book.cost) : book.cost != null) return false;
        if (isbn != null ? !isbn.equals(book.isbn) : book.isbn != null) return false;
        return noOfPages != null ? noOfPages.equals(book.noOfPages) : book.noOfPages == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (cost != null ? cost.hashCode() : 0);
        result = 31 * result + (isbn != null ? isbn.hashCode() : 0);
        result = 31 * result + (noOfPages != null ? noOfPages.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", cost=" + cost +
                ", isbn='" + isbn + '\'' +
                ", noOfPages=" + noOfPages +
                '}';
    }
}

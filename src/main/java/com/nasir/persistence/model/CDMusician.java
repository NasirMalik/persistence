package com.nasir.persistence.model;

import com.nasir.persistence.model.id.CDMusicianId;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "cd_musician")
public class CDMusician {

    @EmbeddedId
    private CDMusicianId id;

    private Character status;
    private String detail;

    public CDMusicianId getId() {
        return id;
    }

    public void setId(CDMusicianId id) {
        this.id = id;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

}

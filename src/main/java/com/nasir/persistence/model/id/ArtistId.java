package com.nasir.persistence.model.id;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class ArtistId implements Serializable {

    public String firstName;
    public String lastName;

    public ArtistId() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "ArtistId{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
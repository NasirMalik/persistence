package com.nasir.persistence.model.id;

import com.nasir.persistence.model.CD;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class EditionId implements Serializable {

    @ManyToOne
    @JoinColumn(name = "id", insertable = false, updatable = false)
    private CD cd;

    private Integer editionNumber;

    public EditionId() {
    }

    public Integer getEditionNumber() {
        return editionNumber;
    }

    public void setEditionNumber(Integer editionNumber) {
        this.editionNumber = editionNumber;
    }

    public CD getCd() {
        return cd;
    }

    public void setCd(CD cd) {
        this.cd = cd;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EditionId that = (EditionId) o;

        if (cd != null ? !cd.equals(that.cd) : that.cd != null) return false;
        return editionNumber != null ? editionNumber.equals(that.editionNumber) : that.editionNumber == null;
    }

    @Override
    public int hashCode() {
        return Objects.hash(cd, editionNumber);
    }

    @Override
    public String toString() {
        return "EditionId{" +
                "id=" + cd.getId() +
                ", editionNumber=" + editionNumber +
                '}';
    }
}

package com.nasir.persistence.model.id;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class CDMusicianId implements Serializable {

    @Column(name = "cd_id")
    private Integer cdId;

    @Column(name = "musician_fName")
    private String musicianFirstName;

    @Column(name = "musician_lName")
    private String musicianLastName;

    public Integer getCdId() {
        return cdId;
    }

    public void setCdId(Integer cdId) {
        this.cdId = cdId;
    }

    public String getMusicianFirstName() {
        return musicianFirstName;
    }

    public void setMusicianFirstName(String musicianFirstName) {
        this.musicianFirstName = musicianFirstName;
    }

    public String getMusicianLastName() {
        return musicianLastName;
    }

    public void setMusicianLastName(String musicianLastName) {
        this.musicianLastName = musicianLastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CDMusicianId that = (CDMusicianId) o;

        if (!cdId.equals(that.cdId)) return false;
        if (musicianFirstName.equals(that.musicianFirstName)) return false;
        return musicianLastName.equals(that.musicianLastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cdId, musicianLastName);
    }

    @Override
    public String toString() {
        return "CDMusicianId{" +
                "cdId=" + cdId +
                ", musicianFirstName=" + musicianFirstName +
                ", musicianLastName=" + musicianLastName +
                '}';
    }
}

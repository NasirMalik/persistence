package com.nasir.persistence.model;

import com.nasir.persistence.model.id.ArtistId;

import javax.persistence.*;

@MappedSuperclass
public abstract class Artist {

    @EmbeddedId
    protected ArtistId id;

    protected String biography;
    protected String contact;

    public ArtistId getId() {
        return id;
    }

    public void setId(ArtistId id) {
        this.id = id;
    }

    public String getFirstName() {
        return id.getFirstName();
    }

    public void setFirstName(String firstName) {
        id.setFirstName(firstName);
    }

    public String getLastName() {
        return id.getLastName();
    }

    public void setLastName(String lastName) {
        id.setLastName(lastName);
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }
}

package com.nasir.persistence.listeners;

import javax.persistence.PostPersist;
import javax.persistence.PrePersist;
import java.util.logging.Logger;

public class LifecycleListener {

    Logger log = Logger.getLogger(this.getClass().getName());

    @PrePersist
    public void pre(Object obj) {
        log.info("Printing PrePersist : " + obj);
    }

    @PostPersist
    public void post(Object obj) {
        log.info("Printing PostPersist : " + obj);
    }

}

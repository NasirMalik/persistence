package com.nasir.persistence.listeners;

import com.nasir.persistence.model.Artist;

import javax.persistence.PrePersist;

public class ValidationListener {

    @PrePersist
    public void validate(Artist artist) {
        if (artist.getContact() == null) {
            System.out.println("Contact would have been better");
        }
        if (artist.getBiography() == null) {
            throw  new IllegalArgumentException("A little biography is required");
        }
    }

}
